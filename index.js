const validate = require('./generated/validate')

const valid = { firstName: 'Peter', lastName: 'Pan', age: 12}
const invalid = { firstName: 1 }

console.log("Using ./generated/validate to check some objects\n")
console.log(check(valid))
console.log(check(invalid))

const schema = require('./schema.json')
const esformatter = require('esformatter')
function checkRequired (propName) {
  return `if(!obj.hasOwnProperty('${propName}')) {
            //retun false
          }`
}
function generateCode (schema) {
  return `module.exports = function validate(obj){
            ${schema.required.map(checkRequired).join('\n')}
            return true;
          }`
}

console.log(generateCode(schema));
console.log(esformatter.format(generateCode(schema)))

function check (value) {
  return `Checking ${JSON.stringify(value)}\n` +
    `${validate(value) ? 'VALID' : 'INVALID'}\n`
}
