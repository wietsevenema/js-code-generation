# A Guide to Generating Boring Code with Node

This is an example project that shows how you can generate readable code with a recent version of node and almost no dependencies, using ES6 template literals.  

Remember, friends don't let friends write code that could have been generated.

In this example I take a JSON schema definition and generate a function that can validate objects at runtime. Off course there are already libraries for that, but that's not the point.

Read more in the blogpost: https://medium.com/@wietsevenema/generate-readable-code-with-node-8741049f82b0

## Usage
```
git clone git@gitlab.com:wietsevenema/js-code-generation.git
cd js-code-generation
yarn
```
Run `yarn start` to run some examples and `yarn test` to run the tests. 
